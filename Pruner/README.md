# Pruner

This package provides program `pruner` that skims NanoAOD files.
Different versions of event selection are available.
They are implemented in dedicated classes and chosen at runtime as specified in command line options.

Example commands:

```sh
pruner input_sim.root --selection Quadlepton --subselection MC \
  --max-events 1000 --is-data 0 -o output --year 2016 \
  --config config/triggers.yaml -v 1
```

Full list of command line options is available via `pruner --help`.
