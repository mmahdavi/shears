#ifndef SHEARS_PRUNER_BIN_PRUNER_H_
#define SHEARS_PRUNER_BIN_PRUNER_H_

#include <algorithm>
#include <memory>
#include <set>
#include <string>
#include <vector>

#include <TChain.h>
#include <TFile.h>
#include <TTreeReader.h>
#include <TH1D.h>

#include "LumiMaskFilter.h"
#include "FilterBase.h"
#include "ExtenderBase.h"


/**
 * \brief Implements skimming of NanoAOD files
 *
 * Trees "Events" from a set of NanoAOD files are copied after applying an event
 * selection, which is implemented in a separate class. Additional information
 * can be included in the output file with the help of extenders.
 */
class Pruner {
 public:
  /**
   * \brief Constructor
   *
   * \param[in] filter  Object implementing event selection for skimming.
   * \param[in] isData  Indicates whether real data or simulation will be
   *                    processed.
   */
  Pruner(std::unique_ptr<FilterBase> &&filter, bool isData = false);

  /// Add an extender to the collection of registered extenders
  void AddExtender(std::unique_ptr<ExtenderBase> &&extender) {
    extenders_.emplace_back(std::move(extender));
  }

  /**
   * \brief Request filtering according to a luminosity mask
   *
   * Only events that match the mask will be kept.
   *
   * \param[in] path  Path to a JSON file with a luminosity mask.
   */
  void ApplyLumiMask(std::string const &path) {
    lumiMaskFilter_.reset(new LumiMaskFilter(path));
  }

  /**
   * \brief Request that branches specified in the given file are not copied
   *
   * The names of the branches to drop must be listed in a YAML file as a
   * sequence.
   */
  void DropBranches(std::string const &path);

  /**
   * \brief Process multiple files
   *
   * \param[in] inputFiles  Paths to file to process.
   * \param[in] outputFile  Name for output file.
   * \param[in] maxEvents   Maximal number of events to process; -1 to disable
   *                        limit.
   * \param[in] skipEvents  Number of events to skip at the beginning.
   */
  void Run(
      std::vector<std::string> const &inputFiles, std::string const &outputFile,
      int maxEvents = -1, int skipEvents = 0);

  /**
   * \brief Process files from a catalogue
   *
   * \param[in] catalogFile  Path to catalogue file.
   * \param[in] outputFile   Name for output file.
   * \param[in] maxEvents    Maximal number of events to process; -1 to disable
   *                         limit.
   * \param[in] skipEvents   Number of events to skip at the beginning.
   * \param[in] maxFiles     Maximal number of files from the catalogue to
   *                         process; -1 to disable limit.
   * \param[in] skipFiles    Number of files from the catalogue to skip at the
   *                         beginning.
   */
  void Run(std::string const &catalogFile, std::string const &outputFile,
           int maxEvents = -1, int skipEvents = 0,
           int maxFiles = -1, int skipFiles = 0);

  /**
   * \brief Set message verbosity level
   *
   * \param[in] value  Verbosity level, 0 for the quitest mode.
   */
  void SetVerbosity(int value) {
    verbose_ = value;
  }

 private:
  /// Fill underlying TChain with files from the given catalogue
  void FillChain(std::string const &catalogPath, int maxFiles = -1,
                 int skipFiles = 0);

  /// Perform initialization with input tree
  void Initialize();

  /// Read next event from input tree
  bool NextEvent();

  /**
   * \brief Loop over input tree "Events", apply event selection, and write data
   * to output file
   */
  void RunImpl();

  /// Set up output file
  void SetOutput(std::string const &outputDataFile);

  /// Object implementing event selection
  std::unique_ptr<FilterBase> filter_;

  /**
   * \brief Registered extenders
   *
   * When running over simulation, a NormalizationExtender is included
   * automatically.
   */
  std::vector<std::unique_ptr<ExtenderBase>> extenders_;

  /// Names of branches that should be dropped in the output tree
  std::set<std::string> branchesToDrop_;

  std::unique_ptr<LumiMaskFilter> lumiMaskFilter_;

  bool isData_;
  int verbose_;

  /// Chain containing trees "Events" from input files
  TChain eventChain_;

  /// Reader for trees "Events" from input files
  TTreeReader eventReader_;

  /// Chain containing trees "Runs" from input files
  TChain runChain_;

  /// Output file
  std::unique_ptr<TFile> outputFile_;

  /**
   * \brief Filtered tree "Events"
   *
   * Owned by the output file.
   */
  TTree *outEventTree_;

  int maxEvents_;
  int skipEvents_;
  int ievent_;
};

#endif  // SHEARS_PRUNER_BIN_PRUNER_H_

