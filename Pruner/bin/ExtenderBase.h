#ifndef SHEARS_PRUNER_BIN_EXTENDERBASE_H_
#define SHEARS_PRUNER_BIN_EXTENDERBASE_H_

#include "TFile.h"
#include "TTreeReader.h"


/**
 * \brief Interface for extenders used in Pruner
 *
 * Extenders can save additional information to the output file.
 */
class ExtenderBase {
 public:
  virtual ~ExtenderBase() = default;

  /**
   * \brief Process single event from the input
   *
   * \param[in] passed  Indicates whether this event passes the event selection.
   */
  virtual void Fill(bool passed) = 0;

  /**
   * \brief Set up input data and output file
   *
   * \param[in] reader      Reader for the tree "Events".
   * \param[in] outputFile  Output ROOT file.
   */
  virtual void Initialize(TTreeReader &reader, TFile *outputFile) = 0;

  /**
   * \brief Write output
   *
   * This method is called after all input data have been processed and before
   * the output file is closed.
   */
  virtual void Write() = 0;
};

#endif  // SHEARS_PRUNER_BIN_EXTENDERBASE_H_

