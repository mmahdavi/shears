#include "Pruner.h"

#include <algorithm>
#include <iomanip>
#include <iostream>
#include <sstream>
#include <stdexcept>

#include <TLeaf.h>
#include <sys/time.h>
#include <yaml-cpp/yaml.h>

#include "NormalizationExtender.h"


Pruner::Pruner(std::unique_ptr<FilterBase> &&filter, bool isData)
    : filter_{std::move(filter)},
      isData_{isData}, verbose_{0},
      eventChain_{"Events"}, runChain_{"Runs"},
      outEventTree_{nullptr},
      maxEvents_{-1}, skipEvents_{0}, ievent_{-1} {}


void Pruner::DropBranches(std::string const &path) {
  YAML::Node node;
  try {
    node = YAML::LoadFile(path);
  } catch (YAML::ParserException) {
    std::cerr << "Failed to parse file with list of branches \"" << path
        << "\" as a YAML file." << std::endl;
    throw;
  }

  if (not node.IsSequence()) {
    std::ostringstream message;
    message << "Error when parsing file with list of branches \""
        << path << "\": its content must be a sequence.";
    throw std::runtime_error(message.str());
  }

  for (auto const &element : node)
    branchesToDrop_.emplace(element.as<std::string>());

  if (verbose_ > 1) {
    std::cout << "Branches that will be dropped:\n";
    for (auto const &branch : branchesToDrop_)
      std::cout << "  " << branch << '\n';
    std::cout << std::flush;
  }
}


void Pruner::Run(
    std::vector<std::string> const &inputFiles, std::string const &outputFile,
    int maxEvents, int skipEvents) {
  maxEvents_ = maxEvents;
  skipEvents_ = skipEvents;
  for (auto const &path : inputFiles) {
    eventChain_.Add(path.c_str());
    runChain_.Add(path.c_str());
  }
  SetOutput(outputFile);
  RunImpl();
}


void Pruner::Run(
    std::string const &catalogFile, std::string const &outputFile,
    int maxEvents, int skipEvents, int maxFiles, int skipFiles) {
  maxEvents_ = maxEvents;
  skipEvents_ = skipEvents;
  FillChain(catalogFile, maxFiles, skipFiles);
  SetOutput(outputFile);
  RunImpl();
}


void Pruner::FillChain(std::string const &catalogPath, int maxFiles,
                       int skipFiles) {
  YAML::Node catalog;
  try {
    catalog = YAML::LoadFile(catalogPath);
  } catch (YAML::ParserException) {
    std::cerr << "Failed to parse catalogue \"" << catalogPath
        << "\" as a YAML file." << std::endl;
    throw;
  }

  if (not catalog.IsSequence()) {
    std::ostringstream message;
    message << "Error when parsing catalogue \"" << catalogPath
        << "\": its content must be a sequence.";
    throw std::runtime_error(message.str());
  }

  int const endIndex = (maxFiles == -1) ?
      catalog.size() : std::min<int>(skipFiles + maxFiles, catalog.size());
  for (int i = skipFiles; i < endIndex; ++i) {
    auto const path = catalog[i].as<std::string>();
    if (verbose_ > 0)
      std::cout << "Adding file \"" << path << "\" to the list of input files."
          << std::endl;
    eventChain_.Add(path.c_str());
    runChain_.Add(path.c_str());
  }
}


void Pruner::Initialize() {
  if (not isData_)
    extenders_.emplace_back(new NormalizationExtender());

  eventReader_.SetTree(&eventChain_);
  filter_->Initialize(eventReader_);
  if (lumiMaskFilter_)
    lumiMaskFilter_->SetSourceBranches(eventReader_);
  for (auto const &extender : extenders_)
    extender->Initialize(eventReader_, outputFile_.get());
}


bool Pruner::NextEvent() {
  ++ievent_;
  Long64_t entryInTree = eventChain_.LoadTree(ievent_);
  switch(entryInTree){
  case -1: //The chain is empty.
    std::cerr << "No event found in input file(s)!\n";
    return false;
  case -2: //The requested entry number is negative or is too large for the chain,
    //       or too large for the large TTree (?).
    return false;
  case -3: //The file corresponding to the entry could not be correctly open
    std::cerr << "Failed to open file containing the event #" << ievent_ << "\n";
    return false;
  case -4: //The TChainElement corresponding to the entry is missing or
    //       the TTree is missing from the file.
    std::cerr << "EventTree was not found in file ";
    if(eventChain_.GetFile()) std::cerr << eventChain_.GetFile()->GetName();
    std::cerr << "\n";
    return false;
  }

  // The following line is required. Else, the contents are not loaded, which
  // fills all unaccessed branches to 0 instead of the actual value.
  // See https://root-forum.cern.ch/t/ttreereader-with-clonetree/31318
  eventChain_.GetEntry(ievent_);

  return eventReader_.SetEntry(ievent_);
}


void Pruner::RunImpl() {
  Initialize();
  if (verbose_ > 0 and (maxEvents_ >= 0 or skipEvents_ != 0))
    std::cout << "Warning: Program was configured to run on subsets of "
        "input files. Tree \"Runs\" is filled on per-file basis and will not "
        "be consistent with the processed subsets of events." << std::endl;

  bool interactive = isatty(fileno(stdout)) ? true : false;
  if (interactive)
    std::cout << "Interactive mode" << std::endl;
  else
    std::cout << "Batch mode" << std::endl;

  int nevts = eventChain_.GetEntries() - skipEvents_;
  if (maxEvents_ >= 0 && nevts > maxEvents_)
    nevts = maxEvents_;
  if (verbose_ > 0)
    std::cout << "Number of events to read: " << nevts << std::endl;

  if (skipEvents_ > 0)
    ievent_ = skipEvents_ - 1;
  else
    ievent_ = -1;

  int nCopied = 0;

  timeval start;
  gettimeofday(&start, 0);
  timeval t;
  time_t smoothed_eat = 0.;

  for (int i = 1; i <= nevts; ++i) {
    NextEvent();
    bool passed = filter_->Filter();
    if (lumiMaskFilter_ and not lumiMaskFilter_->Pass())
      passed = false;

    for (auto const &extender : extenders_)
      extender->Fill(passed);

    if (passed) {
      outEventTree_->Fill();
      ++nCopied;
    }

    const static int step = interactive ? 100 : 100000;
    //begin-of-line character: in interactive we stay on same line,
    //when stdout is a file we go to next line
    const static char bol = interactive ? '\r' : '\n';

    if (i % step==0 || i == nevts) {
      std::cout << bol << "Read: " << std::setw(8) << i << " Copied: "
           << std::setw(8) << nCopied
           << " Acc.: " << std::setw(5)
           << int(10000 * (nCopied / double(i))) / 100. << "%"
           << " Rem.: " << std::setw(8) << (nevts-i)
           << " Total: " << std::setw(8) << nevts;
      timeval t0;
      if (i == step)
        gettimeofday(&t0, 0);
      else if (i >= 2 * step) {
        gettimeofday(&t, 0);
        double remaining = double(nevts - step) / (i - step)
          * ((t.tv_sec - t0.tv_sec) + 1.e-6 * (t.tv_usec - t0.tv_usec));
        time_t eat = int(t0.tv_sec +  1.e-6 * t0.tv_usec + remaining + 0.5);
        if (smoothed_eat == 0) smoothed_eat = eat;
        if (std::abs(smoothed_eat - eat) > 0.1 * remaining)
          smoothed_eat = 0.5 * (smoothed_eat + eat);
        char buf[256];
        strftime(buf, sizeof(buf),  "%a, %d %b %Y %T",
                 localtime(&smoothed_eat));
        std::cout << " ETA: " << std::setw(16) << buf;
      }
      std::cout << std::flush;
    }
  } //next event
  std::cout << "\n";

  outputFile_->cd();
  outputFile_->WriteObject(outEventTree_, outEventTree_->GetName());
  for (auto extender = extenders_.rbegin(); extender != extenders_.rend();
       ++extender)
    (*extender)->Write();
  TTree *runTree = runChain_.CloneTree();
  outputFile_->WriteObject(runTree, runTree->GetName());
  outputFile_->Close();

  gettimeofday(&t, 0);
  double dt = (t.tv_sec - start.tv_sec) + 1.e-6 * (t.tv_usec - start.tv_usec);
  printf("Elapsed time: %.2g sec. %.1f event/sec.\n",  dt, nevts/dt);
  std::cout << std::flush;
}


void Pruner::SetOutput(std::string const &outputDataFile) {
  outputFile_.reset(TFile::Open(outputDataFile.c_str(), "recreate"));
  if (outputFile_->IsZombie())
    throw std::runtime_error("Failed to create output file.");

  std::set<std::string> good_trigs = {
    //"HLT_Mu17_TrkIsoVVL_Mu8_TrkIsoVVL",
    //"HLT_Mu17_TrkIsoVVL_TkMu8_TrkIsoVVL",
    //"HLT_Mu17_TrkIsoVVL_Mu8_TrkIsoVVL_DZ",
    //"HLT_Mu17_TrkIsoVVL_TkMu8_TrkIsoVVL_DZ",
    //"HLT_Ele23_Ele12_CaloIdL_TrackIdL_IsoVL_DZ",
    //"HLT_DoubleEle33_CaloIdL_GsfTrkIdVL",
    //"HLT_DoubleEle33_CaloIdL_MW",
    //"HLT_DoublePhoton60",
    //"HLT_Mu8_TrkIsoVVL_Ele23_CaloIdL_TrackIdL_IsoVL",
    //"HLT_Mu8_TrkIsoVVL_Ele23_CaloIdL_TrackIdL_IsoVL_DZ",
    //"HLT_Mu23_TrkIsoVVL_Ele8_CaloIdL_TrackIdL_IsoVL",
    //"HLT_Mu23_TrkIsoVVL_Ele8_CaloIdL_TrackIdL_IsoVL_DZ",

    "HLT_Mu17_TrkIsoVVL_Mu8_TrkIsoVVL_DZ_Mass8",
    "HLT_Ele23_Ele12_CaloIdL_TrackIdL_IsoVL",
    "HLT_DoubleEle33_CaloIdL_MW",
    "HLT_DoublePhoton70",
    "HLT_Mu23_TrkIsoVVL_Ele12_CaloIdL_TrackIdL_IsoVL_DZ",
    "HLT_Mu8_TrkIsoVVL_Ele23_CaloIdL_TrackIdL_IsoVL_DZ",
    "HLT_Mu23_TrkIsoVVL_Ele12_CaloIdL_TrackIdL_IsoVL"

    //"HLT_Mu17_TrkIsoVVL_Mu8_TrkIsoVVL_DZ_Mass3p8",
    //"HLT_Ele23_Ele12_CaloIdL_TrackIdL_IsoVL",
    //"HLT_DoubleEle25_CaloIdL_MW",
    //"HLT_DoublePhoton70",
    //"HLT_Ele32_WPTight_Gsf",
    //"HLT_Photon200",
    //"HLT_Mu23_TrkIsoVVL_Ele12_CaloIdL_TrackIdL_IsoVL",
    //"HLT_Mu8_TrkIsoVVL_Ele23_CaloIdL_TrackIdL_IsoVL_DZ",
    //"HLT_Mu23_TrkIsoVVL_Ele12_CaloIdL_TrackIdL_IsoVL_DZ",
  };
  for (auto const *branch : *eventChain_.GetListOfBranches()) {
    std::string const branchName{branch->GetName()};
    //if (branchesToDrop_.count(branchName)) {
    if (good_trigs.count(branchName))
        continue;

    if (branchName.find("HLT_") == 0 or branchName.find("L1_") == 0) {
      if (verbose_ > 0)
        std::cout << "Disabling branch \"" << branchName << "\".\n";
      eventChain_.SetBranchStatus(branchName.c_str(), false);
    }
  }

  outEventTree_ = eventChain_.CloneTree(0);
  outEventTree_->SetDirectory(outputFile_.get());
}

