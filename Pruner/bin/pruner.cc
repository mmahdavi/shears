#include <iostream>
#include <map>
#include <memory>
#include <string>
#include <vector>

#include <boost/program_options.hpp>

#include "QuadleptonFilter.h"
#include "Pruner.h"


namespace po = boost::program_options;


void ListSelections(bool asJson = false) {
    std::map<std::string, std::vector<std::string>> selections;
    selections["Quadlepton"] = QuadleptonFilter::SubSelections();

    if (asJson) {
      std::cout << "[";
      bool firstElement = true;
      for (auto const &[selection, subSelections] : selections) {
        for (auto const &subSelection : subSelections) {
          if (not firstElement)
            std::cout << ", ";
          else
            firstElement = false;

          std::cout << "[\"" << selection << "\", \"" << subSelection << "\"]";
        }
      }
      std::cout << "]" << std::endl;
    } else {
      std::cout << "Supported selections and (optionally) subselections:\n";
      for (auto const &[selection, subSelections] : selections) {
        std::cout << "  " << selection << ":\n    ";
        if (subSelections.empty()) {
          std::cout << "(no subselections)";
        } else {
          bool firstElement = true;
          for (auto const &subSelection : subSelections) {
            if (not firstElement)
              std::cout << ", ";
            else
              firstElement = false;
            std::cout << subSelection;
          }
        }
        std::cout << std::endl;
      }
    }
}


int main(int argc, char **argv) {
  std::string outputPath;
  int maxEvents, skipEvents;
  std::string year, configPath, selection, subSelection;

  po::options_description optionsDescription{"Supported options"};
  optionsDescription.add_options()
      ("help,h", "Prints help message.")
      ("verbosity,v", po::value<int>()->default_value(0), "Verbosity level.")
      ("catalog,c", po::value<std::string>(),
       "YAML file with a list of input ROOT files.")
      ("max-events", po::value<int>(&maxEvents)->default_value(-1),
       "Maximum number of events to process.")
      ("skip-events", po::value<int>(&skipEvents)->default_value(0),
       "Number of events to skip at the beginning.")
      ("max-files", po::value<int>()->default_value(-1),
       "Maximum number of files from catalogue to process")
      ("skip-files", po::value<int>()->default_value(0),
       "Number of files to skip a the beginning of catalogue.")
      ("drop-branches", po::value<std::string>(),
       "Drop branches listed in the given YAML file.")
      ("output,o",
       po::value<std::string>(&outputPath)->default_value("output.root"),
       "Name for output file.")
      ("is-data", po::value<bool>()->default_value(false),
       "Specifies whether the input is real data as opposed to simulation.")
      ("year", po::value<std::string>(&year)->default_value("2016"),
       "Year of data taking. Used to adjust set of triggers and IDs.")
      ("config", po::value<std::string>(&configPath)->default_value(""),
       "YAML file with trigger configuration.")
      ("lumi-mask", po::value<std::string>(),
       "Apply luminosity mask from the given JSON file.")
      ("selection", po::value<std::string>(&selection)->required(),
       "Selection to be applied.")
      ("subselection", po::value<std::string>(&subSelection)->default_value(""),
       "Subselection to be applied.")
      ("list-selections",
       "List available selections and subselections and exit.")
      ("json", "Requests that text output is formatted as JSON.");

  po::options_description hiddenOptionsDescription;
  hiddenOptionsDescription.add_options()
      ("input-files", po::value<std::vector<std::string>>(), "");
  po::positional_options_description posOptionsDescription;
  posOptionsDescription.add("input-files", -1);

  po::options_description allOptionsDescription;
  allOptionsDescription.add(optionsDescription).add(hiddenOptionsDescription);
  po::variables_map options;
  po::store(
      po::command_line_parser(argc, argv).options(allOptionsDescription)
          .positional(posOptionsDescription).run(),
      options
  );

  if (options.count("help")) {
    std::cerr << "Usage:\n"
        << "  pruner INPUT_FILE_1 [INPUT_FILE_2 [...]] [OPTIONS]\n"
        << "  pruner -c CATALOG [OPTIONS]\n";
    std::cerr << optionsDescription << std::endl;
    return EXIT_SUCCESS;
  }

  if (options.count("list-selections")) {
    ListSelections(options.count("json"));
    return EXIT_SUCCESS;
  }

  po::notify(options);

  if ((options.count("input-files") > 0) == (options.count("catalog") > 0)) {
    std::cerr << "Input files must be provided either as positional arguments "
        << "or using option --catalog." << std::endl;
    return EXIT_FAILURE;
  }
  int const verbosity = options["verbosity"].as<int>();

  std::unique_ptr<FilterBase> filter;
  if (selection == "Quadlepton")
    filter.reset(new QuadleptonFilter(subSelection, configPath, year, verbosity));
  else {
    std::cerr << "Unrecognized selection \"" << selection << "\"." << std::endl;
    return EXIT_FAILURE;
  }

  Pruner pruner{std::move(filter), options["is-data"].as<bool>()};
  pruner.SetVerbosity(verbosity);
  if (options.count("lumi-mask"))
    pruner.ApplyLumiMask(options["lumi-mask"].as<std::string>());
  if (options.count("drop-branches"))
    pruner.DropBranches(options["drop-branches"].as<std::string>());

  if (options.count("catalog")) {
    pruner.Run(
        options["catalog"].as<std::string>(), outputPath, maxEvents, skipEvents,
        options["max-files"].as<int>(), options["skip-files"].as<int>());
  } else {
    pruner.Run(
        options["input-files"].as<std::vector<std::string>>(),
        outputPath, maxEvents, skipEvents);
  }

  return EXIT_SUCCESS;
}

