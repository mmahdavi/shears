#ifndef SHEARS_PRUNER_BIN_NORMALIZATIONEXTENDER_H_
#define SHEARS_PRUNER_BIN_NORMALIZATIONEXTENDER_H_

#include <optional>

#include <TH1D.h>
#include <TTreeReaderValue.h>

#include "ExtenderBase.h"


/**
 * \brief Extender to save information about normalization
 *
 * Stores number of events read, sums of generator-level weights, and pileup
 * profile. Must only be used with simulation.
 */
class NormalizationExtender : public ExtenderBase {
 public:
  NormalizationExtender();
  void Fill(bool passed) override;
  void Initialize(TTreeReader &reader, TFile *outputFile) override;
  void Write() override;

 private:
  /// Auxiliary structure for bulk initialization of input branches
  struct Source {
    Source(TTreeReader &reader);

    TTreeReaderValue<Float_t> genWeight;
    TTreeReaderValue<Float_t> lheWeight;
    TTreeReaderValue<Float_t> pileupTrueInteractions;
  };

  mutable std::optional<Source> src_;

  /// Non-owning pointer to output file
  TFile *outputFile_;

  int eventsRead_;
  double sumGenWeights_, sumGenWeightsPassed_;
  double sumLheWeights_, sumLheWeightsPassed_;
  TH1D pileupProfile_;
};

#endif  // SHEARS_PRUNER_BIN_NORMALIZATIONEXTENDER_H_

