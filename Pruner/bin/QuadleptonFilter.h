#ifndef SHEARS_PRUNER_BIN_QUADLEPTONFILTER_H_
#define SHEARS_PRUNER_BIN_QUADLEPTONFILTER_H_

#include <optional>
#include <string>
#include <vector>

#include <TLorentzVector.h>
#include <TTreeReaderArray.h>
#include <TTreeReaderValue.h>

#include "FilterBase.h"
#include "TriggerFilter.h"


/**
 * \brief Implements loose quadlepton event selection
 *
 * The trigger selection is determined by the subselection provided to the
 * constructor. Specific triggers are read from a YAML configuration file. In
 * order to be accepted, the event must have fired at least one of the triggers
 * in the group whose name matches the subselection. If the configuration file
 * contains any trigger groups above the selected one, the event must not have
 * fired any of the triggers listed there. This allows ensuring orthogonality
 * between different primary datasets. Special subselection "MC" accepts events
 * by OR of all the triggers in the configuration file.
 */
class QuadleptonFilter : public FilterBase {
 public:
  QuadleptonFilter(
      std::string const &subSelection,
      std::string const &triggerConfigPath, std::string const &year,
      int verbosity = 0);

  bool Filter() const override;
  void Initialize(TTreeReader &reader) override;

  /**
   * \brief Return supported subselections
   *
   * The subselections are not checked against what is actually included in the
   * configuration file.
   */
  static std::vector<std::string> SubSelections();

 private:
  /**
   * \brief Auxiliary structure to simplify delayed initialization of branches
   * read from source tree
   */
  struct Source {
    Source(TTreeReader &reader);

    TTreeReaderValue<UInt_t> elNum;
    TTreeReaderArray<Int_t> elCutBasedId;
    TTreeReaderArray<Float_t> elPt, elEta, elPhi, elMass;
    TTreeReaderValue<UInt_t> muNum;
    TTreeReaderArray<Bool_t> muLooseId;
    TTreeReaderArray<Float_t> muPt, muEta, muPhi, muMass;
  };

  /// Trigger filter that must be passed by event for it to be selected
  TriggerFilter triggersAccept_;

  /// Trigger filter that must not be passed by event for it to be selected
  TriggerFilter triggersVeto_;

  mutable std::optional<Source> src_;
};

#endif  // SHEARS_PRUNER_BIN_QUADLEPTONFILTER_H_
