#include "QuadleptonFilter.h"

#include <algorithm>
#include <sstream>
#include <stdexcept>

#include <boost/filesystem.hpp>
#include <yaml-cpp/yaml.h>

namespace fs = boost::filesystem;


QuadleptonFilter::QuadleptonFilter(
    std::string const &subSelection, std::string const &triggerConfigPath,
    std::string const &year, int verbosity) {
  if (triggerConfigPath.empty())  // By default the path is set to ""
    throw std::runtime_error(
        "Path to trigger configuration file was not provided.");
  if (not fs::exists(triggerConfigPath)) {
    std::ostringstream message;
    message << "Trigger configuration file \""
        << triggerConfigPath << "\" doesn't exist.";
    throw std::runtime_error(message.str());
  }

  auto const triggerConfig =
    YAML::LoadFile(triggerConfigPath)["Quadlepton"][year];
  if (not triggerConfig) {
    std::ostringstream message;
    message << "Trigger configuration file \"" << triggerConfigPath
        << "\" doesn't contain entry for year \"" << year << "\".";
    throw std::runtime_error(message.str());
  }
  if (not triggerConfig.IsSequence()) {
    std::ostringstream message;
    message << "In trigger configuration file \"" << triggerConfigPath
        << "\", expected entry for year \"" << year << "\" to be a sequence.";
    throw std::runtime_error(message.str());
  }

  std::vector<std::string> triggersAccept, triggersVeto;
  for (auto const &groupNode : triggerConfig) {
    if (not groupNode.IsMap() or groupNode.size() != 1) {
      std::ostringstream message;
      message << "In trigger configuration file \"" << triggerConfigPath
          << "\", expected every element in entry for year \"" << year
          << "\" to be a map containing a single element.";
      throw std::runtime_error(message.str());
    }
    auto const node = *groupNode.begin();
    auto const groupName = node.first.as<std::string>();
    auto const groupTriggers = node.second.as<std::vector<std::string>>();

    if (subSelection == "MC") {
      // In simulation just use an OR of all triggers in the configuration
      triggersAccept.insert(
          triggersAccept.end(), groupTriggers.begin(), groupTriggers.end()
      );
    } else if (groupName == subSelection) {
      triggersAccept = groupTriggers;
      break;
    } else {
      // All trigger groups placed before the group corresponding to the
      // requested selection in data should be vetoed
      triggersVeto.insert(
          triggersVeto.end(), groupTriggers.begin(), groupTriggers.end());
    }
  }

  if (triggersAccept.empty()) {
    std::ostringstream message;
    message << "In trigger configuration file \"" << triggerConfigPath
        << "\", the entry for year \"" << year << "\" doesn't contain "
        << "triggers for requested subselection \"" << subSelection << "\".";
    throw std::runtime_error(message.str());
  }
  triggersAccept_.SetTriggers(triggersAccept);
  triggersVeto_.SetTriggers(triggersVeto);

  if (verbosity >= 2) {
    std::cout << "Accept triggers:";
    for (auto const &trigger : triggersAccept)
      std::cout << " " << trigger;
    std::cout << "\nVeto triggers:";
    for (auto const &trigger : triggersVeto)
      std::cout << " " << trigger;
    std::cout << std::endl;
  }
}


bool QuadleptonFilter::Filter() const {
  // Find good leptons of any flavour that pass the pt threshold. Include a 10%
  // margin in pt to allow applying pt scale calibration and evaluating its
  // uncertainties.
  double const minLepPt = 0;
  std::vector<TLorentzVector> leptonMomenta;

  for (int i = 0; i < int(*src_->elNum); ++i) {
    if (src_->elPt[i] < minLepPt)
      continue;

    // While transitioning to a new ID, keep the old one added in OR
    bool const oldId = (src_->elCutBasedId[i] >= 2);  // loose working point
    if (not oldId)
      continue;

    TLorentzVector p4;
    p4.SetPtEtaPhiM(src_->elPt[i], src_->elEta[i], src_->elPhi[i],
                    src_->elMass[i]);
    leptonMomenta.emplace_back(p4);
  }

  for (int i = 0; i < int(*src_->muNum); ++i) {
    if (src_->muPt[i] < minLepPt)
      continue;

    // While transitioning to a new ID, keep the old one added in OR
    bool const oldId = src_->muLooseId[i];
    if (not oldId)
      continue;

    TLorentzVector p4;
    p4.SetPtEtaPhiM(src_->muPt[i], src_->muEta[i], src_->muPhi[i],
                    src_->muMass[i]);
    leptonMomenta.emplace_back(p4);
  }

  // Select events with at least four good leptons, regardless of their flavours.
  if (leptonMomenta.size() < 4)
    return false;

  return triggersAccept_() and not triggersVeto_();
}


void QuadleptonFilter::Initialize(TTreeReader &reader) {
  src_.emplace(reader);

  for (auto f : {&triggersAccept_, &triggersVeto_})
    f->Init(reader);
}


std::vector<std::string> QuadleptonFilter::SubSelections() {
  return {
      "MC", "DoubleEG", "EGamma", "DoubleMuon", "MuonEG"};
}


QuadleptonFilter::Source::Source(TTreeReader &reader)
    : elNum{reader, "nElectron"},
      elCutBasedId{reader, "Electron_cutBased"},
      elPt{reader, "Electron_pt"}, elEta{reader, "Electron_eta"},
      elPhi{reader, "Electron_phi"}, elMass{reader, "Electron_mass"},
      muNum{reader, "nMuon"},
      muLooseId{reader, "Muon_looseId"},
      muPt{reader, "Muon_pt"}, muEta{reader, "Muon_eta"},
      muPhi{reader, "Muon_phi"}, muMass{reader, "Muon_mass"} {}

