# Skimming


## Repository organisation

* `Pruner` Program to perform event filtering in NanoAOD files producing smaller ROOT files (`skims`).
* `Production` Tools run the skimming over Grid.


## Installation recipe

Set up CMSSW:

```sh
cmsrel CMSSW_10_2_22
cd CMSSW_10_2_22/src 
cmsenv 
```

Clone and build this repository:

```sh
git clone ssh://git@gitlab.cern.ch:7999/mmahdavi/shears.git
scramv1 b -j $(nproc)
```

Set up CRAB and obtain proxy for running over Grid:

```sh
source $VO_CMS_SW_DIR/crab3/crab.sh
voms-proxy-init --voms cms
```


## Examples

Producing a skim locally:

```sh
pruner --selection Quadlepton --subselection MC_DLep \
  -o myskim.root --max-events 10000 --is-data=0 \
  --year 2016 --config config/triggers.yaml
  input_nanoaod.root
```
where `input_nanoaod.root` is an input file in the NanoAOD format.

Submitting jobs to Grid:

```sh
crab_submit config.yaml
```

where the format of the configuration file `config.yaml` is described in directory `Production`.

More details are provided in README files in the subdirectories.
