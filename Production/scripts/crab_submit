#!/usr/bin/env python

"""Prepares and submits CRAB tasks."""

from __future__ import division, print_function
import argparse
from httplib import HTTPException
import math
import os
import shutil
import sys

try:
    from CRABAPI.RawCommand import crabCommand
    from CRABClient.ClientExceptions import ClientException
    from WMCore.Configuration import Configuration
except ImportError:
    print('Fatal error: CRAB 3 environment is not set up.', file=sys.stderr)
    sys.exit(1)

from shears.Production.config import Config
import Utilities.General.cmssw_das_client as das_client


pruner_path = os.path.expandvars('$CMSSW_BASE/bin/$SCRAM_ARCH/pruner')


def check_environment():
    for path in [
        '$CMSSW_BASE/src/shears/Production/crab/job_script.py',
        '$CMSSW_BASE/src/shears/Production/crab/haddnano.py',
        '$CMSSW_BASE/src/shears/Production/crab/PSet.py',
        '$CMSSW_BASE/src/shears/Pruner/config/triggers.yaml',
        pruner_path
    ]:
        expanded_path = os.path.expandvars(path)
        if not os.path.isfile(expanded_path):
            raise RuntimeError(
                'Required file "{}" is not found.'.format(expanded_path))


def compute_job_splitting(dataset, events_per_job=10e6):
    """Compute number of files to be processed per job.

    Arguments:
        dataset:    Dataset represented by a config.Dataset object.
        events_per_job:  Desired number of events per job.

    Return value:
        Number of files from the input dataset to be processed per job.
    """

    counts_found = False
    for block in das_lookup(dataset):
        try:
            num_events = block['dataset'][0]['nevents']
            num_files = block['dataset'][0]['nfiles']
        except KeyError:
            continue
        else:
            counts_found = True
            break
    if not counts_found:
        raise RuntimeError(
            'Could not find number of events or files in dataset '
            '"{}" in DAS.'.format(dataset.name))

    events_per_file = num_events / num_files
    return int(math.ceil(min(events_per_job, num_events) / events_per_file))


def das_lookup(dataset):
    """Perform DAS lookup for given dataset.

    Arguments:
        dataset:    Dataset represented by a config.Dataset object.

    Return value:
        'data' field from DAS response.

    Respect DBS instance if it is specified in
    dataset.crab_cfg_extension.
    """

    dbs = None
    try:
        dbs = dataset.crab_cfg_extension['Data']['inputDBS']
    except KeyError:
        pass

    if dbs is None:
        query = 'dataset=' + dataset.name
    else:
        query = 'dataset={} instance=prod/{}'.format(dataset.name, dbs)

    response = das_client.get_data(query)
    if response['status'] == 'ok':
        return response['data']
    else:
        raise RuntimeError(
            'DAS query "{}" resulted in an error.'.format(query))


def write_crab_config(task_name, dataset, selection, year, files_per_job=10,
                      lumi_mask=None, stageout_dir=None,
                      crab_config_path='crabConfig.py'):
    """Create CRAB configuration and save it in a file.

    Arguments:
        task_name:  Name for CRAB task.
        dataset:    Dataset represented by a config.Dataset object.
        selection:  Tuple of selection and subselection for pruner.
        files_per_job:  Number of files to process per job.
        lumi_mask:  Luminosity mask to be applied.  Path to a local file
            or a URL.
        stageout_dir:  LFN for a directory at IIHE where to Grid output
            should be staged out.
        crab_config_path:  Name for CRAB configuration file.

    Return value:
        None.
    """

    crab_dir = os.path.expandvars('$CMSSW_BASE/src/shears/Production/crab')
    pruner_config_dir = os.path.expandvars(
        '$CMSSW_BASE/src/shears/Pruner/config')
    output_file_name = task_name + '.root'

    config = Configuration()

    config.section_('General')
    config.General.requestName = task_name
    config.General.transferOutputs = True
    config.General.transferLogs = True

    config.section_('JobType')
    config.JobType.pluginName = 'Analysis'
    config.JobType.psetName = os.path.join(crab_dir, 'PSet.py')
    config.JobType.scriptExe = os.path.join(crab_dir, 'job_script.py')
    config.JobType.scriptArgs = [
        '--selection=' + selection[0], '--subselection=' + selection[1],
        '--output=' + output_file_name,
        '--is-data={}'.format(0 if dataset.is_sim else 1),
        '--year=' + year,
        '--config=triggers.yaml'
    ]
    config.JobType.inputFiles = [
        pruner_path, os.path.join(crab_dir, 'haddnano.py'),
        os.path.join(pruner_config_dir, 'triggers.yaml')
    ]
    config.JobType.outputFiles = [output_file_name]
    config.JobType.maxJobRuntimeMin = 8 * 60

    config.section_('Data')
    config.Data.inputDataset = dataset.name
    if lumi_mask:
        config.Data.lumiMask = lumi_mask
    config.Data.splitting = 'FileBased'
    config.Data.unitsPerJob = files_per_job
    if stageout_dir:
        config.Data.outLFNDirBase = stageout_dir
    config.Data.publication = False

    config.section_('User')
    config.section_('Site')
    config.Site.storageSite = 'T2_BE_IIHE'

    # Enforce that jobs are submitted only to sites that host input
    # dataset [1]
    # [1] https://twiki.cern.ch/twiki/bin/view/CMSPublic/CRAB3FAQ?rev=105#Why_are_my_jobs_submitted_to_a_s
    config.section_('Debug')
    config.Debug.extraJDL = ['+CMS_ALLOW_OVERFLOW=False']

    # Update the default configuration with per-dataset settings
    for section_name, params in dataset.crab_cfg_extension.iteritems():
        section = getattr(config, section_name)
        for name, value in params.iteritems():
            setattr(section, name, value)

    with open(crab_config_path, 'w') as f:
        f.write(config.pythonise_())


def submit_task(task_name, config_path):
    """Submit a CRAB task.

    Arguments:
        task_name:    Name of the task for logging.
        config_path:  Path to CRAB configuration file for the task.

    Return value:
        None.
    """

    print('Submitting task "\033[1;34m{}\033[0m"...'.format(task_name))
    cur_attempt = 0
    num_attempts = 3
    try_submit = True

    while try_submit:
        cur_attempt += 1
        try_submit = False

        try:
            crabCommand('submit', config=config_path)
        except HTTPException as e:
            print('\033[1;38;5;208mFailed to submit\033[0m '
                  'because of an HTTP exception:')
            print(' ', str(e.headers))

            if cur_attempt <= num_attempts:
                print('Going to try again (attempt {} of {})...'.format(
                    cur_attempt, num_attempts))
                shutil.rmtree('crab_' + task_name)
                try_submit = True
            else:
                print('\033[1;31mGiving up on this task\033[0m')
        except ClientException as e:
            print('\033[01;31mFailed to submit\033[0m '
                  'because of a client exception:')
            print(' ', str(e))


if __name__ == '__main__':
    arg_parser = argparse.ArgumentParser(description=__doc__)
    arg_parser.add_argument(
        'config',
        help='Configuration file with a list of datasets to process and '
        'event selections.')
    arg_parser.add_argument(
        '-n', '--no-submit', action='store_true',
        help='Create CRAB configuration files but do not submit the jobs.')
    args = arg_parser.parse_args()

    check_environment()
    config = Config(args.config)
    config.check_selections(pruner_path)

    for dataset in config.datasets():
        for selection in dataset.selections:
            task_name = dataset.build_task_name(selection)
            crab_config_path = 'crab_{}.py'.format(task_name)
            files_per_job = compute_job_splitting(dataset)
            write_crab_config(
                task_name=task_name, dataset=dataset,
                selection=selection,
                year=config.year(),
                files_per_job=files_per_job,
                lumi_mask=None if dataset.is_sim else config.data_lumi_mask(),
                stageout_dir=config.stageout_dir().lfn,
                crab_config_path=crab_config_path
            )

            if not args.no_submit:
                submit_task(task_name, crab_config_path)
                print()

