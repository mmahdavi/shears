# NanoAOD skimming

Basic event selection on top of NanoAOD is performed using the [`pruner`](https://gitlab.cern.ch/HZZ-IIHE/shears/tree/master/Pruner) utility. Tools provided here allow to run it over multiple datasets in Grid.


## Configuration file

The list of datasets to process is provided in a configuration file like [`config/2016.yaml`](config/2016.yaml), the access to which is provided with interface Python module `config.py`.
Every group is assigned a list of one or more selections, which are represented by pairs of strings defining selection plugin and subselection for `pruner`. Each group also has a name, which must uniquely identify the group within the campaign. It is used to construct names for CRAB tasks, output files, and inherited as the dataset name in the subsequent analysis.

Here is an example entry for a single dataset group:

```yaml
- name: ZZTo4L
  datasets:
  - /ZZTo4L_13TeV_powheg_pythia8/RunIIFall17NanoAODv7-PU2017_12Apr2018_Nano02Apr2020_102X_mc2017_realistic_v8-v1/NANOAODSIM
  - /ZZTo4L_13TeV_powheg_pythia8/RunIIFall17NanoAODv7-PU2017_12Apr2018_Nano02Apr2020_102X_mc2017_realistic_v8_ext1-v1/NANOAODSIM
  selections: [[Quadlepton, MC]]
```

The configuration file also specifies the location to store the output from CRAB jobs.


## Skimming

The `crab_submit` tool can be used for massing production in the Grid as follows:

* Obtain the credentials with

  ```sh
  voms-proxy-init --voms cms
  ```

  and set up [CRAB](https://twiki.cern.ch/twiki/bin/view/CMSPublic/SWGuideCrab) environment. Run

  ```sh
  scram b
  ```

  to make sure that up-to-date scripts are copied to standard locations in CMSSW.
* Switch to a clean working directory:

  ```sh
  mkdir skims
  cd skims
  ```
  
* Adjust the configuration file if needed and run

  ```sh
  crab_submit config.yaml
  ```
  
  You can use flag `--no-submit` to create CRAB configuration files without submitting the tasks.
* Use the standard CRAB command to check the task status or use the script `crab_check`. The latter will check all tasks in the current directory or, alternatively, tasks corresponding to the given configuration file. When a task is completed, its directory will be added to a text file with the list of completed tasks (by default, `completed_tasks.txt`) and the script willl not check its status again. Make sure that there is no such file in the directory when starting a skimming campaign. This script can also resubmit failed tasks, but make sure not to resubmit tasks with unrecoverable errors.


## Dataset definition files

After skimming jobs succeed, [dataset definition files](https://gitlab.cern.ch/HZZ-IIHE/hzz2l2nu/wikis/dataset-definitions) (DDF) should be produced for the subsequent analysis. To do it, update the VOMS proxy if needed and run

```sh
build_ddf config.yaml
```

Produced files are saved in subdirectory `DDF` of the stageout directory specified in the configuration file.


## Pileup profiles

Pileup profiles in data can be constructed by running

```sh
build_pileup_profiles_data --mask lumi_mask.json --pileup pileup.json [-c config.yaml]
```

Here `lumi_mask.json` is the certification file, which would normally also be provided as `data_lumi_mask` in the production configuration file. File `pileup.json` contains delivered luminosity per luminosity block (which allows to compute the pileup knowning the corresponding cross section). This file is produced centrally and described in [this page](https://twiki.cern.ch/twiki/bin/viewauth/CMS/PileupJSONFileforData). Note that both files are usually located at AFS and hence first need to be copied to the IIHE cluster. If the optional production configuration file is given, the produced ROOT file with pileup profiles will be copied to the storage location specified there. The produced ROOT file contains pileup profiles for all data taking eras separately; for each era the nominal profile as well as profiles for a variation in the pileup cross section are provided.

Pileup profiles used in simulation are listed in [this page](https://twiki.cern.ch/twiki/bin/view/CMS/PileupScenariosRun2). The scenario used in a given campaign can be found in [McM](https://cms-pdmv.cern.ch/mcm/) in the configuration of the DIGI-RECO step of an example sample or, with premixing, of the exploited neutrino gun sample. The profile should be saved in a ROOT file as a histogram ([example script](https://gitlab.cern.ch/snippets/1142)).

In 2017, however, pileup profiles for some simulated samples have been affected by a problem, as described [here](https://hypernews.cern.ch/HyperNews/CMS/get/physics-validation/3128.html). After producing the DDF, per-dataset pileup profiles for subsequent analysis should be constructed. To do it, update the VOMS proxy if needed and run

```sh
build_pileup_profiles_sim config.yaml
```

Produced file is saved in subdirectory `pileup` of the stageout directory specified in the configuration file, under the name of `pileup_profiles_sim.root`.
