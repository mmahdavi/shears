#!/bin/bash

cd $CI_BUILDS_DIR

. /cvmfs/cms.cern.ch/cmsset_default.sh
scramv1 project -n CMSSW CMSSW CMSSW_10_2_11

cd CMSSW/src
eval `scramv1 runtime -sh`
scramv1 build

